# Cron Time

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

This simple Drupal module is used to set custom Cron Time.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cron_time).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cron_time).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Goto: /admin/config/system/cron


## Maintainers

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
